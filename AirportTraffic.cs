﻿using SimulacijaAerodroma.ManualPlaneInsertion;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimulacijaAerodroma
{
    public class AirportTraffic
    {
        private ListBox listboxStatus;

        public AirportTraffic(ListBox listboxStatus)
        {
            this.listboxStatus = listboxStatus;
        }

        public void HandlePlaneTransition(Airplane airplane, ListBox initialListBox, ListBox middleListBox, ListBox finalListBox, int initialDelay, int finalDelay, string firstMessage, string finalMessage)
        {
            Task.Run(() =>
            {
                string callsign = airplane.Callsign;
                Thread.Sleep(initialDelay);
                initialListBox.Invoke(new MethodInvoker(() => initialListBox.Items.Remove(airplane)));
                middleListBox.Invoke(new MethodInvoker(() => middleListBox.Items.Add(airplane)));
                listboxStatus.Invoke(new MethodInvoker(() => listboxStatus.Items.Add($"{callsign} {firstMessage}")));
                Thread.Sleep(finalDelay);
                middleListBox.Invoke(new MethodInvoker(() => middleListBox.Items.Remove(airplane)));
                finalListBox.Invoke(new MethodInvoker(() => finalListBox.Items.Add(airplane)));
                listboxStatus.Invoke(new MethodInvoker(() => listboxStatus.Items.Add($"{callsign} {finalMessage}")));
                Thread.Sleep(finalDelay);
                
                if (initialListBox.Name == "lbDepartures")
                {
                    finalListBox.Invoke(new MethodInvoker(() => finalListBox.Items.Remove(airplane)));
                    listboxStatus.Invoke(new MethodInvoker(() => listboxStatus.Items.Add($"{callsign}, see ya later!")));
                }
            });
        }

    }
}
