using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace SimulacijaAerodroma
{
    public partial class AirplaneDB : DbContext
    {
        public AirplaneDB()
            : base("name=EntityDB")
        {
        }

        public virtual DbSet<Table> Tables { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Table>()
                .Property(e => e.Callsign)
                .IsUnicode(false);

            modelBuilder.Entity<Table>()
                .Property(e => e.DepCode)
                .IsUnicode(false);

            modelBuilder.Entity<Table>()
                .Property(e => e.ArrCode)
                .IsUnicode(false);

            modelBuilder.Entity<Table>()
                .Property(e => e.DepTime)
                .IsUnicode(false);
        }
    }
}
