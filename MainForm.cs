using SimulacijaAerodroma.ManualPlaneInsertion;
using System;
using System.Drawing.Text;
using System.IO;
using System.Numerics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Windows.Forms;

namespace SimulacijaAerodroma
{
    public partial class MainForm : Form
    {
        private AirplaneRepository plane;
        private AirportTraffic airportTraffic;
        public MainForm()
        {
            InitializeComponent();
            InitializeDragDrop();
            plane = new AirplaneRepository();
            airportTraffic = new AirportTraffic(lbStatus);

            InitializeDatabase();
        }

        private void InitializeDatabase()
        {
            lbTerminal.Items.Clear();
            foreach (var planes in plane.All())
            {
                Airplane planeString = new Airplane(planes.Callsign, planes.DepCode, planes.ArrCode, planes.DepTime, planes.Remark);
                lbTerminal.Items.Add(planeString);
            }
        }

        public ListBox TerminalListBox
        {
            get { return lbTerminal; }
        }
        public ListBox InboundListBox
        {
            get { return lbInbound; }
        }

        private void InitializeDragDrop()
        {

            lbInbound.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbInbound.AllowDrop = true;
            lbInbound.DragOver += new DragEventHandler(ListBox_DragOver);
            lbInbound.DragDrop += new DragEventHandler(ListBox_DragDrop);

            lbLand.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbLand.AllowDrop = true;
            lbLand.DragOver += new DragEventHandler(ListBox_DragOver);
            lbLand.DragDrop += new DragEventHandler(ListBox_DragDrop);

            lbTerminal.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbTerminal.AllowDrop = true;
            lbTerminal.DragOver += new DragEventHandler(ListBox_DragOver);
            lbTerminal.DragDrop += new DragEventHandler(ListBox_DragDrop);

            lbDepartures.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbDepartures.AllowDrop = true;
            lbDepartures.DragOver += new DragEventHandler(ListBox_DragOver);
            lbDepartures.DragDrop += new DragEventHandler(ListBox_DragDrop);

            lbTaxiingOut.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbTaxiingOut.AllowDrop = true;
            lbTaxiingOut.DragOver += new DragEventHandler(ListBox_DragOver);
            lbTaxiingOut.DragDrop += new DragEventHandler(ListBox_DragDrop);

            lbTakeoff.MouseDown += new MouseEventHandler(ListBox_MouseDown);
            lbTakeoff.AllowDrop = true;
            lbTakeoff.DragOver += new DragEventHandler(ListBox_DragOver);
            lbTakeoff.DragDrop += new DragEventHandler(ListBox_DragDrop);
        }

        private void ListBox_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.SelectedItem == null) return;

            //var plane = lb.SelectedItem as ManualPlaneInsertion.Airplane;
            var planeString = lb.SelectedItem as Airplane;

            var data = new DataObject();
            /*if (plane != null)
            {
                data.SetData(typeof(ManualPlaneInsertion.Airplane), plane);
            }*/
            if (planeString != null)
            {
                data.SetData(typeof(Airplane), planeString);
            }

            data.SetData(typeof(ListBox), lb);
            lb.DoDragDrop(data, DragDropEffects.Move);
        }


        private void ListBox_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
        private void ListBox_DragDrop(object sender, DragEventArgs e)
        {
            ListBox targetListBox = sender as ListBox;
            if (targetListBox == null) return;

            var planeString = e.Data.GetData(typeof(Airplane)) as Airplane;
            var sourceListBox = e.Data.GetData(typeof(ListBox)) as ListBox;

            try
            {
                if (planeString != null && sourceListBox != null)
                {
                    //PO�INJU IF STATEMENTI :)
                    if (sourceListBox == lbTerminal && targetListBox == lbTakeoff)
                    {
                        throw new InvalidOperationException("WARNING! TAKEOFF NOT ALLOWED! \n\nCLEAR DEPARTURE FIRST!");
                    }

                    if (sourceListBox == lbTerminal && targetListBox == lbTaxiingOut)
                    {
                        throw new InvalidOperationException("WARNING! DEPARTURE HAS TO BE CLEARED!");
                    }
                    
                    if (sourceListBox == lbTerminal && targetListBox == lbLand)
                    {
                        throw new InvalidOperationException("WARNING! NOT ABLE TO DO THAT >:(");
                    }

                    if (sourceListBox == lbTerminal && targetListBox == lbInbound)
                    {
                        throw new InvalidOperationException("WHAT ARE YOU DOING!?!?");
                    }

                    if (sourceListBox == lbTerminal && targetListBox == lbTaxiingIn)
                    {
                        throw new InvalidOperationException("MAKES NO SENSE...");
                    }


                    if (sourceListBox == lbInbound && targetListBox == lbTaxiingIn)
                    {
                        throw new InvalidOperationException("WARNING! PLANE HAS TO CLEAR LANDING/LAND FIRST");
                    }

                    if (sourceListBox == lbInbound && targetListBox == lbTaxiingOut)
                    {
                        throw new InvalidOperationException("HOW CAN YOU DEPARTURE IF IT ISN'T EVEN ON THE GROUND YET?? :/");
                    }

                    if (sourceListBox == lbInbound && targetListBox == lbDepartures)
                    {
                        throw new InvalidOperationException("BRUH, IT HASN'T EVEN LANDED YET");
                    }

                    if (sourceListBox == lbInbound && targetListBox == lbTakeoff)
                    {
                        throw new InvalidOperationException("CHILL, IT HASN'T EVEN BEEN CLEARED FOR LANDING");
                    }

                    if (sourceListBox == lbInbound && targetListBox == lbTerminal)
                    {
                        throw new InvalidOperationException("NO SHORTCUTS!");
                    }




                    targetListBox.Items.Add(planeString);
                    sourceListBox.Items.Remove(planeString);

                    if (targetListBox == lbLand && targetListBox.Items.Count > 2)
                    {
                        throw new InvalidOperationException("WARNING! STRIPS OCCUPIED!");
                    }



                    else if (sourceListBox == lbInbound && targetListBox == lbLand)
                    {
                        airportTraffic.HandlePlaneTransition(planeString, lbLand, lbTaxiingIn, lbTerminal, 1000, 1000, "Standby", "Arrived at gate");
                    }
                    else if (sourceListBox == lbTerminal && targetListBox == lbDepartures)
                    {
                        airportTraffic.HandlePlaneTransition(planeString, lbDepartures, lbTaxiingOut, lbTakeoff, 1000, 1000, "Ready for Takeoff", "Cleared for takeoff");
                        plane.DeletePlane(planeString);
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (sourceListBox != null && planeString != null)
                {
                    sourceListBox.Items.Add(planeString);
                }
                if (targetListBox.Items.Count != 0)
                {
                    targetListBox.Items.RemoveAt(targetListBox.Items.Count - 1);
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            InputForm form2 = new InputForm(this);
            form2.Show();
        }

        private void lbDepartures_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void lblStatus_Click(object sender, EventArgs e)
        {

        }

        private void lbInbound_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSerializer_Click(object sender, EventArgs e)
        {
            List<string> ParkedPlanes = new List<string>();

            foreach (var item in lbTerminal.Items)
            {
                ParkedPlanes.Add(item.ToString());
            }

            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ParkedPlanes.json");
            string jsonString = JsonSerializer.Serialize(ParkedPlanes);

            File.WriteAllText(fileName, jsonString);
            System.Diagnostics.Process.Start("notepad.exe", fileName);

        }
    }
}
