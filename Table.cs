namespace SimulacijaAerodroma
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Table")]
    public partial class Table
    {
        public int Id { get; set; }

        [StringLength(6)]
        public string Callsign { get; set; }

        [StringLength(3)]
        public string DepCode { get; set; }

        [StringLength(3)]
        public string ArrCode { get; set; }

        [StringLength(5)]
        public string DepTime { get; set; }

        [StringLength(20)]
        public string Remark { get; set; }
        
        public override string ToString()
        {
            return $" {Callsign} |{DepCode}|{ArrCode}|{DepTime}| {Remark}";
        }
        
    }
}
