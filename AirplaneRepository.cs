﻿using SimulacijaAerodroma.ManualPlaneInsertion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimulacijaAerodroma
{
    public class AirplaneRepository
    {

        public AirplaneRepository()
        {
            database = new AirplaneDB();
        }

        public IEnumerable<Table> All()
        {
            return
                from plane in database.Tables
                orderby plane.Id descending
                select plane;

        }

        public void AddPlane(Airplane plane)
        {
            Table newAirplane = new Table
            {
                Callsign = plane.Callsign,
                DepCode = plane.Departure,
                ArrCode = plane.Arrival,
                DepTime = plane.DepTime,
                Remark = plane.Remark 
            };

            database.Tables.Add(newAirplane);
            database.SaveChanges();
        }

        
        public void DeletePlane(Airplane airplane)
        {
            var planeToDelete = database.Tables.FirstOrDefault(plane => plane.Callsign == airplane.Callsign);
            if (planeToDelete != null)
            {
                database.Tables.Remove(planeToDelete);
                database.SaveChanges();
            }
        }
    
        private AirplaneDB database;
    }
}
