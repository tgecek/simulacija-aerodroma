﻿using SimulacijaAerodroma.ManualPlaneInsertion;

namespace SimulacijaAerodroma
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSerializer = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.ListBox();
            this.lbTaxiingOut = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbTakeoff = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbTaxiingIn = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbDepartures = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbTerminal = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbLand = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.lbInbound = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(338, 26);
            this.label1.TabIndex = 3;
            this.label1.Text = "TWR INB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(718, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(340, 26);
            this.label2.TabIndex = 7;
            this.label2.Text = "DEPARTURES";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(356, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(340, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "CLEARED TO LAND";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(1069, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(340, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "TERMINAL";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Open Sans Extrabold", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(10, 471);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 38);
            this.button1.TabIndex = 10;
            this.button1.Text = "Manual Insertion";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(356, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(340, 26);
            this.label5.TabIndex = 12;
            this.label5.Text = "TAXIING INBOUND";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(718, 409);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(340, 26);
            this.label6.TabIndex = 14;
            this.label6.Text = "CLEARED FOR TAKEOFF";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSerializer
            // 
            this.btnSerializer.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSerializer.Location = new System.Drawing.Point(1283, 469);
            this.btnSerializer.Name = "btnSerializer";
            this.btnSerializer.Size = new System.Drawing.Size(126, 40);
            this.btnSerializer.TabIndex = 26;
            this.btnSerializer.Text = "Log";
            this.btnSerializer.UseVisualStyleBackColor = true;
            this.btnSerializer.Click += new System.EventHandler(this.btnSerializer_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Open Sans Extrabold", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(718, 213);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(340, 26);
            this.label7.TabIndex = 28;
            this.label7.Text = "TAXIING OUTBOUND";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbStatus
            // 
            this.lbStatus.FormattingEnabled = true;
            this.lbStatus.Location = new System.Drawing.Point(356, 460);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(340, 121);
            this.lbStatus.TabIndex = 29;
            // 
            // lbTaxiingOut
            // 
            this.lbTaxiingOut.BackColor = System.Drawing.Color.DimGray;
            this.lbTaxiingOut.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbTaxiingOut.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbTaxiingOut.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbTaxiingOut.FormattingEnabled = true;
            this.lbTaxiingOut.ItemHeight = 50;
            this.lbTaxiingOut.Location = new System.Drawing.Point(718, 242);
            this.lbTaxiingOut.Name = "lbTaxiingOut";
            this.lbTaxiingOut.Size = new System.Drawing.Size(340, 154);
            this.lbTaxiingOut.TabIndex = 27;
            // 
            // lbTakeoff
            // 
            this.lbTakeoff.BackColor = System.Drawing.Color.DimGray;
            this.lbTakeoff.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbTakeoff.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbTakeoff.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbTakeoff.FormattingEnabled = true;
            this.lbTakeoff.ItemHeight = 50;
            this.lbTakeoff.Location = new System.Drawing.Point(718, 438);
            this.lbTakeoff.Name = "lbTakeoff";
            this.lbTakeoff.Size = new System.Drawing.Size(340, 154);
            this.lbTakeoff.TabIndex = 13;
            // 
            // lbTaxiingIn
            // 
            this.lbTaxiingIn.BackColor = System.Drawing.Color.DimGray;
            this.lbTaxiingIn.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbTaxiingIn.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbTaxiingIn.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbTaxiingIn.FormattingEnabled = true;
            this.lbTaxiingIn.ItemHeight = 50;
            this.lbTaxiingIn.Location = new System.Drawing.Point(356, 242);
            this.lbTaxiingIn.Name = "lbTaxiingIn";
            this.lbTaxiingIn.Size = new System.Drawing.Size(340, 204);
            this.lbTaxiingIn.TabIndex = 11;
            // 
            // lbDepartures
            // 
            this.lbDepartures.BackColor = System.Drawing.Color.DimGray;
            this.lbDepartures.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbDepartures.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbDepartures.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbDepartures.FormattingEnabled = true;
            this.lbDepartures.ItemHeight = 50;
            this.lbDepartures.Location = new System.Drawing.Point(718, 42);
            this.lbDepartures.Name = "lbDepartures";
            this.lbDepartures.Size = new System.Drawing.Size(340, 154);
            this.lbDepartures.TabIndex = 6;
            this.lbDepartures.SelectedIndexChanged += new System.EventHandler(this.lbDepartures_SelectedIndexChanged);
            // 
            // lbTerminal
            // 
            this.lbTerminal.BackColor = System.Drawing.Color.DimGray;
            this.lbTerminal.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbTerminal.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbTerminal.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbTerminal.FormattingEnabled = true;
            this.lbTerminal.ItemHeight = 50;
            this.lbTerminal.Location = new System.Drawing.Point(1069, 41);
            this.lbTerminal.Name = "lbTerminal";
            this.lbTerminal.Size = new System.Drawing.Size(340, 404);
            this.lbTerminal.TabIndex = 5;
            // 
            // lbLand
            // 
            this.lbLand.BackColor = System.Drawing.Color.DimGray;
            this.lbLand.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbLand.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbLand.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbLand.FormattingEnabled = true;
            this.lbLand.ItemHeight = 50;
            this.lbLand.Location = new System.Drawing.Point(356, 41);
            this.lbLand.Name = "lbLand";
            this.lbLand.Size = new System.Drawing.Size(340, 154);
            this.lbLand.TabIndex = 4;
            // 
            // lbInbound
            // 
            this.lbInbound.BackColor = System.Drawing.Color.DimGray;
            this.lbInbound.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbInbound.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbInbound.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lbInbound.FormattingEnabled = true;
            this.lbInbound.ItemHeight = 50;
            this.lbInbound.Location = new System.Drawing.Point(10, 41);
            this.lbInbound.Name = "lbInbound";
            this.lbInbound.Size = new System.Drawing.Size(340, 404);
            this.lbInbound.TabIndex = 20;
            this.lbInbound.SelectedIndexChanged += new System.EventHandler(this.lbInbound_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Open Sans Extrabold", 8F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(10, 448);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 23);
            this.label8.TabIndex = 30;
            this.label8.Text = "Inbound plane";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1421, 604);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbTaxiingOut);
            this.Controls.Add(this.btnSerializer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbTakeoff);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbTaxiingIn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbDepartures);
            this.Controls.Add(this.lbTerminal);
            this.Controls.Add(this.lbLand);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbInbound);
            this.Name = "MainForm";
            this.Text = "Control board";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private CustomListBox lbInbound;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Button button1;
        private Label label5;
        private Label label6;
        private CustomListBox lbLand;
        private CustomListBox lbTerminal;
        private CustomListBox lbDepartures;
        private CustomListBox lbTaxiingIn;
        private CustomListBox lbTakeoff;
        private Button btnSerializer;
        private Label label7;
        private CustomListBox lbTaxiingOut;
        private ListBox lbStatus;
        private Label label8;
    }
}
