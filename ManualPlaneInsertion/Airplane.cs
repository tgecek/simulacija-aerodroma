﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulacijaAerodroma.ManualPlaneInsertion
{
    public class Airplane
    {
        public Airplane(string callsign, string departure, string arrival, string depTime, string remark)
        {
            Callsign = callsign;
            Departure = departure;
            Arrival = arrival;
            DepTime = depTime;
            Remark = remark;
        }

        public override string ToString()
        {
            return string.Format(" {0} |{1}|{2}| {3} | {4}", Callsign, Departure, Arrival, DepTime, Remark);
        }

        public string Callsign { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public string DepTime { get; set; }
        public string ArrTime { get; set; }

        public string Remark { get; set; }

    }
}
