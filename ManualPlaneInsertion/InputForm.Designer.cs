﻿using SimulacijaAerodroma.ManualPlaneInsertion;
using SimulacijaAerodroma;

namespace SimulacijaAerodroma
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCall = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDepCode = new System.Windows.Forms.TextBox();
            this.tbArrCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbComm = new System.Windows.Forms.CheckBox();
            this.cbCargo = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbAirplanes = new SimulacijaAerodroma.ManualPlaneInsertion.CustomListBox();
            this.tbDepTime = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // tbCall
            // 
            this.tbCall.Location = new System.Drawing.Point(10, 35);
            this.tbCall.Name = "tbCall";
            this.tbCall.Size = new System.Drawing.Size(182, 20);
            this.tbCall.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.DarkGray;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(10, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(182, 26);
            this.label4.TabIndex = 10;
            this.label4.Text = "Callsign:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGray;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(10, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 26);
            this.label1.TabIndex = 11;
            this.label1.Text = "Dep airport code:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.DarkGray;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(10, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 26);
            this.label2.TabIndex = 12;
            this.label2.Text = "Arr airport code:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbDepCode
            // 
            this.tbDepCode.Location = new System.Drawing.Point(10, 86);
            this.tbDepCode.Name = "tbDepCode";
            this.tbDepCode.Size = new System.Drawing.Size(182, 20);
            this.tbDepCode.TabIndex = 13;
            // 
            // tbArrCode
            // 
            this.tbArrCode.Location = new System.Drawing.Point(10, 137);
            this.tbArrCode.Name = "tbArrCode";
            this.tbArrCode.Size = new System.Drawing.Size(182, 20);
            this.tbArrCode.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.DarkGray;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(10, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 26);
            this.label3.TabIndex = 15;
            this.label3.Text = "Departure time:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbComm
            // 
            this.cbComm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cbComm.Location = new System.Drawing.Point(10, 228);
            this.cbComm.Name = "cbComm";
            this.cbComm.Size = new System.Drawing.Size(182, 32);
            this.cbComm.TabIndex = 17;
            this.cbComm.Text = "Commercial Plane";
            this.cbComm.UseVisualStyleBackColor = true;
            // 
            // cbCargo
            // 
            this.cbCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cbCargo.Location = new System.Drawing.Point(10, 265);
            this.cbCargo.Name = "cbCargo";
            this.cbCargo.Size = new System.Drawing.Size(182, 32);
            this.cbCargo.TabIndex = 18;
            this.cbCargo.Text = "Cargo Plane";
            this.cbCargo.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAdd.Font = new System.Drawing.Font("Open Sans Extrabold", 12F);
            this.btnAdd.Location = new System.Drawing.Point(10, 302);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(182, 38);
            this.btnAdd.TabIndex = 19;
            this.btnAdd.Text = "Add Plane";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // lbAirplanes
            // 
            this.lbAirplanes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbAirplanes.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.lbAirplanes.FormattingEnabled = true;
            this.lbAirplanes.ItemHeight = 50;
            this.lbAirplanes.Location = new System.Drawing.Point(218, 22);
            this.lbAirplanes.Name = "lbAirplanes";
            this.lbAirplanes.Size = new System.Drawing.Size(471, 304);
            this.lbAirplanes.TabIndex = 20;
            // 
            // tbDepTime
            // 
            this.tbDepTime.Location = new System.Drawing.Point(10, 188);
            this.tbDepTime.Name = "tbDepTime";
            this.tbDepTime.Size = new System.Drawing.Size(182, 20);
            this.tbDepTime.TabIndex = 21;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(699, 367);
            this.Controls.Add(this.tbDepTime);
            this.Controls.Add(this.lbAirplanes);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.cbCargo);
            this.Controls.Add(this.cbComm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbArrCode);
            this.Controls.Add(this.tbDepCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCall);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "InputForm";
            this.Text = "Manual insertion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox tbCall;
        private Label label4;
        private Label label1;
        private Label label2;
        private TextBox tbDepCode;
        private TextBox tbArrCode;
        private Label label3;
        private CheckBox cbComm;
        private CheckBox cbCargo;
        private Button btnAdd;
        private CustomListBox lbAirplanes;
        private MaskedTextBox tbDepTime;
    }
}