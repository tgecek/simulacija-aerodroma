﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SimulacijaAerodroma.ManualPlaneInsertion
{

    public class CustomListBox : ListBox
    {
        public CustomListBox()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
            ItemHeight = 50;
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            base.OnDrawItem(e);

            if (e.Index < 0 || e.Index >= Items.Count)
                return;

            var item = Items[e.Index].ToString();

            Color bgColor = Color.LightBlue;

            e.DrawBackground();
            using (var brush = new SolidBrush(bgColor))
            {
                Rectangle rect = new Rectangle(e.Bounds.X + 5, e.Bounds.Y + 5, e.Bounds.Width - 10, e.Bounds.Height - 10);
                e.Graphics.FillRectangle(brush, rect);
            }

            TextRenderer.DrawText(e.Graphics, item, e.Font, e.Bounds, e.ForeColor, TextFormatFlags.Left | TextFormatFlags.VerticalCenter);
        }
    }

}
