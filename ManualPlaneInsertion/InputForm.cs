﻿using SimulacijaAerodroma.ManualPlaneInsertion;
using System;
using System.Windows.Forms;

using SimulacijaAerodroma.ManualPlaneInsertion;
using System;
using System.Windows.Forms;

namespace SimulacijaAerodroma
{
    public partial class InputForm : Form
    {
        //private PlaneRepository planeRepository;
        private AirplaneRepository airplaneRepository;
        private MainForm mainInterface;

        public InputForm(MainForm mainInterface)
        {
            InitializeComponent();
            //planeRepository = new PlaneRepository();
            airplaneRepository = new AirplaneRepository();

            //PRESTAO MI JE RADIT "ADD PLANE" BUTTON PA SAM MORO OVO DODAT :/
            btnAdd.Click += btnAdd_Click;

            this.mainInterface = mainInterface;
            tbCall.MaxLength = 6;
            tbDepCode.MaxLength = 3;
            tbArrCode.MaxLength = 3;
            tbDepCode.Text = "ZAG";
            tbDepTime.Mask = "00:00";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Button clicked");
            if (cbCargo.Checked && cbComm.Checked)
            {
                MessageBox.Show("Izaberite isključivo jednu kategoriju", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cbCargo.Checked)
            {
                string callsign = tbCall.Text.PadRight(6);
                string depCode = tbDepCode.Text.PadRight(3);
                string arrCode = tbArrCode.Text.PadRight(3);
                if (!IsValidTime(tbDepTime.Text))
                {
                    MessageBox.Show("Neispravan format vremena", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Airplane plane = new Airplane(callsign, depCode, arrCode, tbDepTime.Text, "Cargo");
                airplaneRepository.AddPlane(plane);
                RefreshAirplaneList(plane);
            }
            else if (cbComm.Checked)
            {
                string callsign = tbCall.Text.PadRight(6);
                string depCode = tbDepCode.Text.PadRight(3);
                string arrCode = tbArrCode.Text.PadRight(3);
                if (!IsValidTime(tbDepTime.Text))
                {
                    MessageBox.Show("Neispravan format vremena", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Airplane plane = new Airplane(callsign, depCode, arrCode, tbDepTime.Text, "Commercial");
                airplaneRepository.AddPlane(plane);
                RefreshAirplaneList(plane);
            }
            else
            {
                MessageBox.Show("Unesite sve podatke", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
        }

        private void RefreshAirplaneList(Airplane plane)
        {
            foreach (var airplane in airplaneRepository.All())
            {
                lbAirplanes.Items.Add(airplane);
            }
            mainInterface.InboundListBox.Items.Add(plane);
        }

        private bool IsValidTime(string time)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(time, @"^(?:[01]?[0-9]|2[0-3]):[0-5][0-9]$");
        }
    }
}
